
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



function getBMI() {
    var teza=document.getElementById("teza").value; //dobimo vrednost iz text polja "teza" in shranimo kot spremenljivko
    var visina=document.getElementById("visina").value; // -||- "visina"
    if(teza==""|| visina==""){ //pogledamo če česa ni vnesel user 
        alert("Prosim, vnesite vse podatke!"); //javimo naj doda
        return;
    }
    visina=visina/100; 
    var BMI=(teza/visina)/visina; //dobimo BMI
    if(BMI<18.5){
        document.getElementById("bmi").value="Vaš BMI, "+(BMI).toFixed(1)+", je prenizek!"; //text polju "bmi" določimo vrednost (spremenljivka BMI+nek text da je lepše) to je izpis pač
        
    }
    
    if(BMI>=18.5 && BMI<=25){
        document.getElementById("bmi").value="Vaš BMI, "+(BMI).toFixed(1)+", je optimalen!";
        
    }
    
    if(BMI>25){
        document.getElementById("bmi").value="Vaš BMI, "+(BMI).toFixed(1)+", je previsok!";
        
    }
    
    if(BMI>35){
        document.getElementById("bmi").value="Vaš BMI, "+(BMI).toFixed(1)+", je nevarno previsok!";
        alert("Vaš BMI je nevarno visok, čimprej se posvetujte z zdravnikom!");
        
    }
}

function dnevneKalorije(){
    var teza=document.getElementById("teza").value; //tukaj spet dobimo vrednosti iz polj
    var visina=document.getElementById("visina").value;
    var spol=document.getElementById("spol").value;
    var aktivnost= document.getElementById("aktivnost").value;
    var starost=document.getElementById("starost").value;
    var BMI=(teza/(visina/100))/(visina/100);
    
    if(teza==""||visina==""||spol=="void"||aktivnost=="void"||starost==""||BMI==""){ //spet check
        alert("Prosim, vnesite vse podatke!");
        return;
    }
    
    if (spol== "Moški"){
        var BMR=88.362+(13.397*teza)+(4.799*visina)-(5.677*starost); //formula
        
        
        
        if(BMI>=18.5 && BMI<=25){ //use normalno, kok rabi da obdrži težo
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2;
                    document.getElementById("bmr").value=(BMR).toFixed(0); //spet izpišemo vrednost
                    break;
                case "Light":
                    BMR=BMR*1.3;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Intenzivno":
                    BMR=BMR*1.725;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                
            }
        }
        
        
        else if(BMI<18.5){ //underweight
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2+300;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Light":
                    BMR=BMR*1.3+300;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55+300;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                    
                case "Intenzivno":
                    BMR=BMR*1.725+300;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
            }
        
        }
        
        else if(BMI>25){ //overweight/obese
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Light":
                    BMR=BMR*1.3-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Intenzivno":
                    BMR=BMR*1.725-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
            }
        
        }
            
        }
    
    
    if (spol=="Ženski"){ // isto samo za ženske
        var BMR=447.593+(9.247*teza)+(3.098*visina)-(4.330*starost);
        
        if(BMI>=18.5 && BMI<=25){ //use normalno, kok rabi da obdrži težo
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Light":
                    BMR=BMR*1.3;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Intenzivno":
                    BMR=BMR*1.725;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                
            }
        }
        
        
        else if(BMI<18.5){
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2+175;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Light":
                    BMR=BMR*1.3+175;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55+175;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                    
                case "Intenzivno":
                    BMR=BMR*1.725+175;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
            }
        
        }
        
        else if(BMI>25){
            switch(aktivnost){
                case "Nič":
                    BMR=BMR*1.2-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Light":
                    BMR=BMR*1.3-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Zmerno":
                    BMR=BMR*1.55-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
                case "Intenzivno":
                    BMR=BMR*1.725-500;
                    document.getElementById("bmr").value=(BMR).toFixed(0);
                    break;
            }
        
        }
     
    
    }
    
    
    function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}











    
    
//TODO: nastavi vrednost BMR, da se pokaže,
// fucking EHR naredi, da dela (vpis, postanje BMI, get BMI, izračun za random pacienta BMI)
// da bo lepo use
//END TODO
}



function kreirajEHR() { //funkcija za vpis
	sessionId = getSessionId();

	var ime = document.getElementById("ime").value; //vrednosti za vpis
	var priimek = document.getElementById("priimek").value;
	var datumRojstva = document.getElementById("rojstvo").value;

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		alert("Prosim, vnesite potrebne podatke!");
		return;
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", //API klic
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                 alert("EHR uspešno ustvarjen, vaš EHR ID je: " + ehrId);
		                    //$("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}





function shraniPodatke() { //shranimo podatke na EHR
	sessionId = getSessionId();

	var ehrId = document.getElementById("EHRid").value;
	var datumInUra = "";
	var telesnaVisina = document.getElementById("visina").value;;
	var telesnaTeza = document.getElementById("teza").value;;
	var telesnaTemperatura = 0; //teh podatkov js ne rabim, zato nastavim na 0
	var sistolicniKrvniTlak = 0;
	var diastolicniKrvniTlak = 0;
	var nasicenostKrviSKisikom =0;
	var merilec ="";

	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim, vnesite Vaš EHR ID!");
		return;
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	/*"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom*/
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        alert("Podatki so bili uspešno shranjeni v EHR")
		    },
		    error: function(err) {
		    	
            alert(JSON.parse(err.responseText).userMessage );
		    }
		});
	}
}






function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() { //funkcija ki vrne izbrano vrednost
	sessionId = getSessionId();
	
	

	var ehrId = document.getElementById("vnesiID").value;
	var tip = "telesna teža"; //vračamo samo težo tako da ja
	

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
	alert("Prosim, vnesite EHR ID");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje telesne teže uporabnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				
				if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    	alert("Ni podatkov");
					    	}
					    },
					    error: function() {
					        alert(JSON.parse(err.responseText).userMessage + "'!");
					    	
					    }
					});
				} 
	    	},
	    	
		});
	}
}


$(document).ready(function() { //ta funkcija nardi,da se napolnijo vrednosti ob kliku na dropdown

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#vnesiID").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		document.getElementById("EHRid").value=podatki[0];
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() { //ta tudi kot prejšnja .change
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});

function generirajPodatke(stPacienta) {
	ehrId = "";
	return ehrId;
}





